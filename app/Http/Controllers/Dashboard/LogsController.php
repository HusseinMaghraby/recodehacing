<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LogsController extends Controller
{
    //

    public function index(){

        return view('dashboard.sections.login');

    }

    public function store(Request $request){

        $credentials = $request->only('email', 'password');
//        dd($credentials);
        if (Auth::attempt($credentials)) {
//            dd($credentials);
            // Authentication passed...
            return redirect()->intended('dashboard');
        }

        return "its not working";
    }
}
