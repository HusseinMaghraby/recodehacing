<?php

namespace App\Http\Controllers\Dashboard;

use App\Photo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MediasController extends Controller
{
    //

    public function index(){

        $photos = Photo::all();

        return view('dashboard.sections.media.index', compact('photos'));
    }

    public function create(){

        return view('dashboard.sections.media.create');
    }

    public function store(Request $request){

        $file = $request->file('file');

        $name = time() . $file->getClientOriginalName();

        $file->move('images', $name);

        Photo::create(['file'=>$name]);
    }

    public function destroy($id){

        $photo = Photo::findOrFail($id);

        $path =  parse_url($photo->file);

        unlink(public_path($path['path']));

        $photo->delete();

        return redirect()->route('media.index');
    }
}