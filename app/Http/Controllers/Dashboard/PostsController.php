<?php

namespace App\Http\Controllers\Dashboard;

use App\Category;
use App\Http\Requests\Requests\PostCreateRequest;
use App\Photo;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $posts = Post::all();

        return view('dashboard.sections.posts.index', compact('posts', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::pluck('name', 'id')->all();

        return view('dashboard.sections.posts.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostCreateRequest $request)
    {
        //
        $input = $request->except('photo_id');
        $input['user_id'] =Auth::user()->id;
//        $user = Auth::user();

        if( $request->photo_id){
            $file =$request->photo_id;
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $photo = photo::create(['file'=>$name]);
            $input['photo_id'] = $photo->id;
        }
//        dd($input);
        post::create($input);
        return redirect('dashboard/posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $post = Post::findOrFail($id);

        $categories = Category::pluck('name', 'id')->all();

        return view('dashboard.sections.posts.edit', compact('post', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->all();
        if( $request->photo_id){
            $file =$request->photo_id;
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $photo = photo::create(['file'=>$name]);
            $input['photo_id'] = $photo->id;
        }

        Auth::user()->posts()->whereId($id)->first()->update($input);

        return redirect('dashboard/posts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $post =  Post::findOrFail($id);

        $path = parse_url($post->photo->file);


        unlink(public_path($path['path']));

        $post->delete();


        return redirect()->route('posts.index');
    }
}
