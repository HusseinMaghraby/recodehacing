<!-- start header -->
<section class="header  fixed-top">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 col-xs-12 logo">
                <a href="/dashboard">
                    <h1 class="text-center">d3aw
                        <span id="move">a</span>
                    </h1>
                </a>
            </div>
            <!-- col -->
            <div class="col-md-10 col-xs-12 name">
                <div id="button" aria-expanded="true" data-toggle="collapse" class=" navbar-toggler">
                    <a class="pull-left bars" href="#" data-target="#sidebar">
                        <span class="icon"></span>
                    </a>

                </div>

                <ul class="nav navbar-nav navbar-right pull-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ route('admin.login') }}">Login</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="color: #fff;">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu" >
                                <li>
                                    <a href="{{ route('admin.logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>










            </div>
            <!-- col -->
        </div>
        <!-- row -->
    </div>
    <!-- container -->
</section>
<!-- end header -->
