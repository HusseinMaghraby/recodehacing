<div class="col-md-2 float-left  collapse  width show  fixed-top" id="sidebar">
    <div class="list-group border-0 card text-center text-md-left ">

        <!-- start dashboard -->
        <a style="    border-left: 3px solid #dd1818;" href="{{route('dashboard.index')}}" class="list-group-item d-inline-block collapsed" data-parent="#sidebar">
            <i class="fa fa-dashboard"></i>
            <span>Dashboard

            </span>

        </a>
        <!-- end dashboard -->

        <!-- start users -->
        <a href="#menu1" class="list-group-item d-inline-block collapsed" data-toggle="collapse">
            <i class="fa fa-users"></i>
            <span href="#menu1" aria-expanded="false" data-toggle="collapse">users </span>

        </a>
        <div class="collapse" id="menu1">
            <a href="{{route('users.index')}}" class="list-group-item child" data-parent="#menu1">
                <i class="fa fa-eye child"></i>view users</a>
            <a href="{{route('users.create')}}" class="list-group-item child" data-parent="#menu1">
                <i class="fa fa-edit child"></i>add users</a>
        </div>
        <!-- end users -->

        <!-- start Posts -->
        <a href="#menu2" class="list-group-item d-inline-block collapsed" data-toggle="collapse">
            <i class="fa fa-pencil-square"></i>
            <span href="#menu2" aria-expanded="false" data-toggle="collapse">Posts
                <!-- <i class="fa fa-arrow-left"></i> -->
            </span>

        </a>
        <div class="collapse" id="menu2">
            <a href="{{route('posts.index')}}" class="list-group-item child" data-parent="#menu2">
                <i class="fa fa-eye child"></i>view posts</a>
            <a href="{{route('posts.create')}}" class="list-group-item child" data-parent="#menu2">
                <i class="fa fa-edit child"></i>add posts</a>
        </div>
        <!-- end Posts -->

        <!-- start categories -->
        <a href="#menu3" class="list-group-item d-inline-block collapsed" data-toggle="collapse">
            <i class="fa fa-list"></i>
            <span href="#menu3" aria-expanded="false" data-toggle="collapse">categories
                <!-- <i class="fa fa-arrow-left"></i> -->
            </span>
        </a>
        <div class="collapse" id="menu3">
            <a href="{{route('categories.index')}}" class="list-group-item child" data-parent="#menu3">
                <i class="fa fa-eye child"></i>view categories</a>
        </div>
        <!-- end categories -->

        <!-- start Media -->
        <a href="#menu4" class="list-group-item d-inline-block collapsed" data-toggle="collapse">
            <i class="fa fa-camera-retro"></i>
            <span href="#menu4" aria-expanded="false" data-toggle="collapse">Media
                <!-- <i class="fa fa-arrow-left"></i> -->
            </span>

        </a>
        <div class="collapse" id="menu4">
            <a href="{{route('media.index')}}" class="list-group-item child" data-parent="#menu4">
                <i class="fa fa-eye child"></i>view Media</a>
            <a href="{{route('media.create')}}" class="list-group-item child" data-parent="#menu4">
                <i class="fa fa-edit child"></i>add Media</a>
        </div>
        <!-- end Media -->

        <!-- start services -->
        <a href="#menu5" class="list-group-item d-inline-block collapsed" data-toggle="collapse">
            <i class="fa fa-cogs"></i>
            <span href="#menu5" aria-expanded="false" data-toggle="collapse">services </span>

        </a>
        <div class="collapse" id="menu5">
            <a href="view.html" class="list-group-item child" data-parent="#menu5">
                <i class="fa fa-eye child"></i>view services</a>
            <a href="addCategories.html" class="list-group-item child" data-parent="#menu5">
                <i class="fa fa-edit child"></i>add services</a>
        </div>
        <!-- end services -->
        <a href="contact.html" class="list-group-item d-inline-block collapsed" data-parent="#sidebar">
            <i class="fa fa-phone"></i>
            <span>contact</span>
        </a>

        <a href="notifiction.html" class="list-group-item d-inline-block collapsed" data-parent="#sidebar">
            <i class="fa fa-sliders"></i>
            <span>setting</span>
        </a>
        <!-- start notifiction -->
        <a href="notifiction.html" class="list-group-item d-inline-block collapsed" data-parent="#sidebar">
            <i class="fa fa-bell-o"></i>
            <span>notifiction</span>
        </a>
        <!-- end notifiction -->
        <!-- start contact info -->
        <a href="contactinfo.html" class="list-group-item d-inline-block collapsed" data-parent="#sidebar">
            <i class="fa fa-share-square-o  "></i>
            <span>contact info</span>
        </a>
        <!-- end contact info -->

    </div>
</div>