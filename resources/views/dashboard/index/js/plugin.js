// start full screen
$(document).ready(function() {
  $(".navbar-toggler").click(function() {
    $(".main").toggleClass("up");
  });
});
// end full screen

// start full screen
$(document).ready(function() {
  $(".navbar-toggler").click(function() {
    $("#sidebar").toggleClass("open");
  });
});
// end full screen
// start remove span
$(document).ready(function() {
  $(".navbar-toggler").click(function() {
    $("#sidebar span").toggleClass("remove");
  });
});

// start export file
$(document).ready(function() {
  $("#example").DataTable({
    dom: "Bfrtip",
    buttons: ["copy", "csv", "excel", "pdf", "print"]
  });
});
// end export file
