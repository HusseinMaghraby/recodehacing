@extends('dashboard.layouts.app')

@section('title', 'D3awa/Edit Categories')

@section('content')

    <div class=" col-md-10 float-right  col px-5 pl-md-2 pt-2 main">

        <div class="add">

            <h5>Categories</h5>

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{route('dashboard.index')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{route('categories.index')}}">Categories</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Edit {{$category->name}}</li>
                </ol>
            </nav>
        </div>
        <!-- add -->
        <div class="parent">



            {!! Form::model($category, ['method'=>'PATCH', 'action'=>['Dashboard\CategoriesController@update', $category->id]]) !!}
            <div class="form-group">
                {!! Form::label('name', 'Name:') !!}
                {!! Form::text('name', null, ['class'=>'form-control']) !!}
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::submit('Update Category', ['class'=>'btn btn-primary col-md-12']) !!}
                    </div>
                    {!! Form::close() !!}

                </div>
                <div class="col-md-6">


                    {!! Form::open(['method'=>'DELETE', 'action'=>['Dashboard\CategoriesController@destroy', $category->id]]) !!}
                    <div class="form-group">
                        {!! Form::submit('Delete Category', ['class'=>'btn btn-danger col-md-12']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
    </div>
@stop
















