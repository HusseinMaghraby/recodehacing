@extends('dashboard.layouts.app')

@section('title', 'D3awa/Categories')

@section('content')

    <div class=" col-md-10 float-right  col px-5 pl-md-2 pt-2 main">

        <div class="add">

            <h5>Categories</h5>

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{route('dashboard.index')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Categories</li>
                </ol>
            </nav>
        </div>
        <!-- add -->
        <div class="parent">

            <div class="row">
                <div class="col-md-6">

                    {!! Form::open(['method'=>'POST', 'action'=>'Dashboard\CategoriesController@store']) !!}
                    <div class="form-group">
                        {!! Form::label('name', 'Name:') !!}
                        {!! Form::text('name', null, ['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::submit('Creat Category', ['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}

                </div>

                <div class="col-md-6">
                    @if($categories)
                        <table class="table">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>Name</th>
                                <th>Created date</th>
                                <th>Control</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $category)
                                <tr>
                                    <td>{{$category->id}}</td>
                                    <td>{{$category->name}}</td>
                                    <td>{{$category->created_at ? $category->created_at->diffForHumans() : 'no date'}}</td>
                                    <td>
                                        <a href="{{route('categories.edit', $category->id)}}"><i class="fa fa-pencil-square-o"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop














