@extends('dashboard.layouts.app')

@section('title', 'D3awa')

@section('content')
    <div class=" col-md-10 float-right  col px-5 pl-md-2 pt-2 main">



        <div class="add">


            <h5>Dashboard</h5>

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{route('dashboard.index')}}">Dashboard</a>
                    </li>
                </ol>
            </nav>
        </div>
        <!-- add -->
        <div class="parent">
            <div class="row">
                <div class="col-md-3 col-xs-12">
                    <div id="first" class="panel ">
                        <i class="fa fa-briefcase fa-4x bag"></i>
                        <div class="orders pull-right text-center">
                            <h2>150</h2>
                            <p>new orders</p>
                        </div>
                        <!-- orders -->
                        <a href="#">
                            <div class="more ">
                                <h6>more info
                                    <i class="fa fa-arrow-circle-right"></i>
                                </h6>
                            </div>
                        </a>
                        <!-- more -->
                    </div>
                    <!-- panel -->
                </div>
                <!-- col-md-4 -->
                <div class="col-md-3 col-xs-12">
                    <div id="second" class="panel">
                        <i class="fa fa-bar-chart fa-4x bag"></i>
                        <div class="orders pull-right text-center">
                            <h2>53%</h2>
                            <p>bounce rate</p>
                        </div>
                        <!-- orders -->
                        <a href="#">
                            <div class="more ">
                                <h6>more info
                                    <i class="fa fa-arrow-circle-right"></i>
                                </h6>
                            </div>
                        </a>
                        <!-- more -->
                    </div>
                    <!-- panel -->
                </div>
                <!-- col-md-4 -->
                <div class="col-md-3 col-xs-12">
                    <div id="third" class="panel ">
                        <i class="fa fa-user-plus fa-4x bag"></i>
                        <div class="orders pull-right text-center">
                            <h2>44</h2>
                            <p>User Registrations</p>
                        </div>
                        <!-- orders -->
                        <a href="#">
                            <div class="more ">
                                <h6>more info
                                    <i class="fa fa-arrow-circle-right"></i>
                                </h6>
                            </div>
                        </a>
                        <!-- more -->
                    </div>
                    <!-- panel -->
                </div>
                <!-- col-md-4 -->


                <div class="col-md-3 col-xs-12">
                    <div id="fourth" class="panel ">
                        <i class="fa fa-pie-chart  fa-4x bag"></i>
                        <div class="orders pull-right text-center">
                            <h2>65</h2>
                            <p>Unique Visitors</p>
                        </div>
                        <!-- orders -->
                        <a href="#">
                            <div class="more ">
                                <h6>more info
                                    <i class="fa fa-arrow-circle-right"></i>
                                </h6>
                            </div>
                        </a>
                        <!-- more -->
                    </div>
                    <!-- panel -->
                </div>
                <!-- col-md-4 -->
            </div>
            <!-- row 2 -->



        </div>
        <!-- col -->
    </div>


@stop



