@extends('dashboard.layouts.app')

@section('title', 'D3awa/Media')

@section('content')

    <div class=" col-md-10 float-right  col px-5 pl-md-2 pt-2 main">

        <div class="add">

            <h5>Media</h5>

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{route('dashboard.index')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Media</li>
                </ol>
            </nav>
        </div>
        <!-- add -->
        <div class="parent">

            @if($photos)

                <table class="table">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Created</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($photos as $photo)

                        <tr>
                            <td>{{$photo->id}}</td>
                            <td><img height="50" src="{{$photo->file}}" alt=""></td>
                            <td>{{$photo->created_at ? $photo->created_at : 'no date'}}</td>
                            <td>
                                {!! Form::open(['method'=>'DELETE', 'action'=>['Dashboard\MediasController@destroy', $photo->id]]) !!}

                                <div class="form-group">
                                    {!! Form::submit('Delete Photo', ['class'=>'btn btn-danger']) !!}
                                </div>
                                {!! Form::close() !!}
                            </td>
                        </tr>

                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@stop






















