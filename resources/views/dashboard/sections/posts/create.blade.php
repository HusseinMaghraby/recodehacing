@extends('dashboard.layouts.app')

@section('title', 'D3awa/Posts Create')

@section('content')

    <div class=" col-md-10 float-right  col px-5 pl-md-2 pt-2 main">

        <div class="add">

            <h5>Posts Create</h5>

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{route('dashboard.index')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{route('posts.index')}}">Posts</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Create</li>
                </ol>
            </nav>
        </div>
        <!-- add -->


        {!! Form::open(['method'=>'POST', 'action'=>'Dashboard\PostsController@store','files'=>true, 'class'=>'kara']) !!}
        <div class="form-group">
            {!! Form::label('title', 'Title:') !!}
            {!! Form::text('title', null, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('category_id', 'Category:') !!}
            {!! Form::select('category_id', [''=>'Choose Categories'] + $categories, null, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('photo_id', 'photo:') !!}
            {!! Form::file('photo_id', ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('body', 'Description:') !!}
            {!! Form::textarea('body', null, ['class'=>'form-control', 'rows'=>3]) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Creat Post', ['class'=>'btn btn-primary']) !!}
        </div>
        {!! Form::close() !!}

        @include('includes.form_error')
    </div>


@stop
































