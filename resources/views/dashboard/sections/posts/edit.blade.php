@extends('dashboard.layouts.app')

@section('title', 'D3awa/Posts Edit')

@section('content')

    <div class=" col-md-10 float-right  col px-5 pl-md-2 pt-2 main">

        <div class="add">

            <h5>Posts Edit</h5>

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{route('dashboard.index')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{route('posts.index')}}">Posts</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Edit</li>
                </ol>
            </nav>
        </div>
        <!-- add -->
        <div class="row">

            <div class="col-sm-3">
                <div class="container">
                    <img src="{{$post->photo ? $post->photo->file : 'http://placehold.it/400x400'}}" alt="" class="img-responsive img-rounded col-md-12">
                </div>
            </div>

            <div class="col-sm-9">

                {!! Form::model($post, ['method'=>'PATCH', 'action'=>['Dashboard\PostsController@update', $post->id],'files'=>true, 'class'=>'kara']) !!}
                <div class="form-group">
                    {!! Form::label('title', 'Title:') !!}
                    {!! Form::text('title', null, ['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('category_id', 'Category:') !!}
                    {!! Form::select('category_id', [''=>'Choose Categories'] + $categories, null, ['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('photo_id', 'photo:') !!}
                    {!! Form::file('photo_id', ['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('body', 'Description:') !!}
                    {!! Form::textarea('body', null, ['class'=>'form-control', 'rows'=>3]) !!}
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::submit('Edit Post', ['class'=>'btn btn-primary col-md-12']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::open(['method'=>'DELETE', 'action'=>['Dashboard\PostsController@destroy', $post->id]]) !!}
                        <div class="form-group">
                            {!! Form::submit('Delete Post', ['class'=>'btn btn-danger col-md-12']) !!}
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}


                @include('includes.form_error')
            </div>
        </div>
    </div>





@stop
