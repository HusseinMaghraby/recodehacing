@extends('dashboard.layouts.app')

@section('title', 'D3awa/Posts')

@section('content')

    <div class=" col-md-10 float-right  col px-5 pl-md-2 pt-2 main">

        <div class="add">

            <h5>Posts</h5>

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{route('dashboard.index')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Posts</li>
                </ol>
            </nav>
        </div>
        <!-- add -->
        <div class="parent">
            <table class="table">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Photo</th>
                    <th>User</th>
                    <th>Category</th>
                    <th>Title</th>
                    <th>body</th>
                    <th>Created</th>
                    <th>Updated</th>
                    <th>control</th>
                </tr>
                </thead>
                @if($posts)
                    @foreach($posts as $post)
                        <tbody>
                        <tr>
                            <td>{{$post->id}}</td>
                            <td><img height="50" src="{{$post->photo ? $post->photo->file : 'http://placehold.it/400x400'}}" alt="""></td>
                            <td>{{$post->user->name}}</td>
                            <td>{{$post->category ? $post->category->name : 'uncategorized'}}</td>
                            <td>{{$post->title}}</td>
                            <td>{{str_limit($post->body, 30)}}</td>
                            <td>{{$post->created_at->diffForHumans()}}</td>
                            <td>{{$post->updated_at->diffForHumans()}}</td>
                            <td>
                                <a href="{{route('posts.edit', $post->id)}}"><i class="fa fa-pencil-square-o"></i></a>
                            </td>
                        </tr>
                        </tbody>
                    @endforeach
                @endif
            </table>
        </div>

    </div>





@stop
