@extends('dashboard.layouts.app')

@section('title', 'D3awa/Users Create')


@section('content')

    <div class=" col-md-10 float-right  col px-5 pl-md-2 pt-2 main">

        <div class="add">


            <h5>Create Users</h5>

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{route('dashboard.index')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{route('users.index')}}">Users</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Create</li>
                </ol>
            </nav>
        </div>
        <!-- add -->

        {!! Form::open(['method'=>'POST', 'action'=>'Dashboard\UsersController@store', 'files'=>true, 'class'=>'kara']) !!}

        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name', null,['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('email', 'Email:') !!}
            {!! Form::email('email', null, ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('role_id', 'Role:') !!}
            <select class="form-control" name="role_id">
                @foreach(\App\Role::all() as $role)
                    <option value="{{$role->id}}">{{$role->name}}</option>
                @endforeach
            </select>
            {{--{!! Form::select('role_id', [''=>'Choose Options'], null, ['class'=>'form-control']) !!}--}}
        </div>

        <div class="form-group">
            {!! Form::label('is_active', 'Status:') !!}
            {!! Form::select('is_active', array(1 => 'Active', 0=> 'Not Active'), 0, ['class'=>'form-control'])!!}
        </div>

        <div class="form-group">
            {!! Form::label('photo_id', 'photo:') !!}
            {!! Form::file('photo_id', ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('password', 'password:') !!}
            {!! Form::password('password', null, ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Create User', ['class'=>'btn btn-primary']) !!}
        </div>

        {!! Form::close() !!}


        @include('includes.form_error')
    </div>



@stop

{{--return '$2y$10$q6g/6jjV3cc6CMGtj2f2fuLGJCNsPAx91zMLyLo3dtaFjICf0cGxu'--}}
{{--return '$2y$10$q6g/6jjV3cc6CMGtj2f2fuLGJCNsPAx91zMLyLo3dtaFjICf0cGxu'--}}





































