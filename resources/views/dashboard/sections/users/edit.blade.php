@extends('dashboard.layouts.app')

@section('title', 'D3awa/Users Edit')

@section('content')

    <div class=" col-md-10 float-right  col px-5 pl-md-2 pt-2 main">

        <div class="add">


            <h5>Edit Users</h5>

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{route('dashboard.index')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{route('users.index')}}">Users</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Edit {{$user->name}}</li>
                </ol>
            </nav>
        </div>
        <!-- add -->

        <div class="row">

            <div class="col-sm-3">
                <div class="container">
                    <img src="{{$user->photo ? $user->photo->file : 'http://placehold.it/400x400'}}" alt="" class="img-responsive img-rounded col-md-12">
                </div>
            </div>


            <div class="col-sm-9">

                {!! Form::model($user, [ 'method'=>'PATCH', 'action'=>['Dashboard\UsersController@update', $user->id], 'files'=>true , 'class'=>'kara']) !!}

                <div class="form-group">
                    {!! Form::label('name', 'Name:') !!}
                    {!! Form::text('name', null,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('email', 'Email:') !!}
                    {!! Form::email('email', null, ['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('role_id', 'Role:') !!}
                    <select class="form-control" name="role_id">
                        @foreach(\App\Role::all() as $role)
                            <option value="{{$role->id}}">{{$role->name}}</option>
                        @endforeach
                    </select>
                    {{--{!! Form::select('role_id', [''=>'Choose Options'], null, ['class'=>'form-control']) !!}--}}
                </div>

                <div class="form-group">
                    {!! Form::label('is_active', 'Status:') !!}
                    {!! Form::select('is_active', array(1 => 'Active', 0=> 'Not Active'), null, ['class'=>'form-control'])!!}
                </div>

                <div class="form-group">
                    {!! Form::label('photo_id', 'photo:') !!}
                    {!! Form::file('photo_id', ['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('password', 'password:') !!}
                    {!! Form::password('password', null, ['class'=>'form-control']) !!}
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::submit('Edit User', ['class'=>'btn btn-primary col-md-12']) !!}

                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::open(['method'=>'DELETE', 'action'=>['Dashboard\UsersController@destroy', $user->id]]) !!}

                        <div class="form-group">
                            {!! Form::submit('Delete User', ['class'=>'btn btn-danger col-md-12']) !!}
                        </div>
                    </div>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
        @include('includes.form_error')
    </div>



@stop







































