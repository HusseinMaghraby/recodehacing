@extends('dashboard.layouts.app')

@section('title', 'D3awa/Users')

@section('content')

    <div class=" col-md-10 float-right  col px-5 pl-md-2 pt-2 main">

        <div class="add">

            <h5>Users</h5>

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{route('dashboard.index')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Users</li>
                </ol>
            </nav>
        </div>
        <!-- add -->
        <div class="parent">
            <span>Data Table With Full Features</span>
            <div class="form-group col-md-6  pull-right">

                <input type="search" class="form-control" id="exampleInputEmail1" placeholder="search">
            </div>

            <table class="table">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>photo</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Status</th>
                    <th>Created</th>
                    <th>Updated</th>
                    <th>Control</th>
                </tr>
                </thead>
                <tbody>

                @if($users)

                    @foreach($users as $user)
                        <tr>
                            <td>{{$user->id}}</td>
                            <td><img height="50" src="{{$user->photo ? $user->photo->file : 'http://placehold.it/400x400'}}" alt="""></td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->role['name']}}</td>
                            <td>{{$user->is_active == 1 ? 'Active' : 'Not Active'}}</td>
                            <td>{{$user->created_at->diffForHumans()}}</td>
                            <td>{{$user->updated_at->diffForHumans()}}</td>
                            <td>
                                <a href="{{route('users.edit', $user->id)}}"><i class="fa fa-pencil-square-o"></i></a>
                            </td>





                        </tr>


                    @endforeach

                @endif
                </tbody>
            </table>
        </div>
    </div>
    </div>


@stop




















