<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('admin/login', 'Dashboard\LogsController@index')->name('admin.login');
Route::post('admin/login', 'Dashboard\LogsController@store')->name('admin.login');

Route::group(['middleware'=>'admin'], function(){

    Route::resource('dashboard/comment/replies','Dashboard\CommentRepliesController');
    Route::resource('dashboard/comments',       'Dashboard\CommentsController');
    Route::resource('dashboard/media',          'Dashboard\MediasController');
    Route::resource('dashboard/categories',     'Dashboard\CategoriesController');
    Route::resource('dashboard/posts',          'Dashboard\PostsController');
    Route::resource('dashboard/users',          'Dashboard\UsersController');
    Route::resource('dashboard/admin',          'Dashboard\AdminController');
    Route::resource('dashboard',                'Dashboard\DashboardController');
    Route::post('dashboard/logout', 'Dashboard\LoginController@logout')->name('admin.logout');
});